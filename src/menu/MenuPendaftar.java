package menu;

import data.DataPendaftar;
import java.util.Scanner;
import static menu.Menu.clearScreen;
import static menu.Menu.tampilan;

public class MenuPendaftar {
    
    Menu menuawal;
    int indeks = 0, kesempatan_login = 3;
    DataPendaftar dataAkun = new DataPendaftar();
    Scanner input = new Scanner(System.in);
    
    public MenuPendaftar() {
        tampilkanMenuAwal();
    }
    
    
    
    void tampilkanMenuAwal(){
        
        boolean loop = true;
        while(loop){
            tampilan('+','=', 50);
            System.out.println("| 1. Daftar Akun                                   |");
            System.out.println("| 2. Login Akun                                    |");
            System.out.println("| 3. Lupa Password?                                |");
            System.out.println("| 4. Kembali ke menu awal                          |");
            tampilan('+','=', 50);
            
            System.out.print("Masukan Pilihan : ");
            String opsi = input.nextLine();
    
            switch (opsi) {
                case "1":
                    loop = false;
                    clearScreen();
                    daftarAkun();
                    break;
                    
                case "2":
                    loop = false;
                    clearScreen();
                    login();
                    break;
                    
                case "3":
                    loop = false;
                    clearScreen();
                    break;
                    
                case "4":
                    loop = false;
                    clearScreen();
                    menuawal = new Menu();
                    break;
                    
                default:
                    System.err.println("Pilihan Tidak ada");
                    break;
            }
            
        }
    }
    
    void daftarAkun(){
        String nama, nik, no_kk, 
            tanggal, bulan, tahun, 
            email, password;
        
        System.out.print("\nNama Lengkap: ");
        nama = input.nextLine();
        
        System.out.print("\nNIK: ");
        nik = input.nextLine();
        
        System.out.print("\nNo. KK: ");
        no_kk = input.nextLine();
        
        System.out.print("\nTanggal lahir: ");
        tanggal = input.nextLine();
        
        System.out.print("\nBulan lahir: ");
        bulan = input.nextLine();
        
        System.out.print("\nTahun lahir: ");
        tahun = input.nextLine();

        System.out.print("\nAlamat Email: ");
        email = input.nextLine();
        
        System.out.print("\nBuat Password Akun: ");
        password = input.nextLine();
        
        DataPendaftar.tambahData(nama, nik, no_kk, tanggal, bulan, tahun, email, password);
        
        System.out.print("Penambahan data sukses\n\n\n");
        tampilkanMenuAwal();
    }
    
    void login(){
        
        boolean loginLoop = true, loginStatus = false;
        
        while ( loginLoop ){
            System.out.println("Kesempatan login: " + kesempatan_login );
            System.out.print("Email    : ");
            String id = input.nextLine();
            
            System.out.print("\nPassword : ");
            String pass = input.nextLine();
            
            for (int i=0; i< DataPendaftar.getArrayPendaftar().size();i++){
                if ( DataPendaftar.getArrayIndex(i).getEmail().equals( id ) && 
                        DataPendaftar.getArrayIndex(i).getPassword().equals( pass )) {
                    indeks=i;
                    loginStatus = true;
                }
            }
            if( loginStatus == true ){
                loginLoop = false;
                tampilkanMenu( indeks );
                    
            }else{
                kesempatan_login -= 1;
                System.out.println(" Email / Password anda salah");
            }

            if( kesempatan_login == 0 ){
                loginLoop = false;
                menuawal = new Menu();
            }
	}
    }
    
    void tampilkanMenu( int indeks ){
        System.out.println(" Selamat Datang, " + DataPendaftar.getArrayIndex(indeks).getNama());
        boolean loop = true;
        while(loop){
            tampilan('+','=', 50);
            System.out.println("| 1. Mulai Tes                                     |");
            System.out.println("| 2. Lihat Data Akun                               |");
            System.out.println("| 3. Ubah Data Akun                                |");
            System.out.println("| 4. Kembali ke menu awal                          |");
            tampilan('+','=', 50);
            
            System.out.print("Masukan Pilihan : ");
            String opsi = input.nextLine();
    
            switch (opsi) {
                case "1":
                    loop = false;
                    clearScreen();
                    System.out.println("***SOAL TES COMING SOON***");
                    tampilkanMenu(indeks);
                    break;
                    
                case "2":
                    loop = false;
                    clearScreen();
                    dataAkun.lihatData(indeks);
                    tampilkanMenu(indeks);
                    break;
                    
                case "3":
                    loop = false;
                    clearScreen();
                    dataAkun.updateData(indeks);
                    tampilkanMenu(indeks);
                    break;
                    
                case "4":
                    loop = false;
                    clearScreen();
                    menuawal = new Menu();
                    break;
                    
                default:
                    System.err.println("Pilihan Tidak ada");
                    break;
            }
            
        }
    }
}
