package data;
import java.util.ArrayList;
public class Pendaftar {
    
    private String nama, nik, no_kk, tanggal, bulan, 
            tahun, email, password;

    public Pendaftar(
            String nama, String nik, String no_kk, 
            String tanggal, String bulan, String tahun, 
            String email, String password) {
        
        this.nama = nama;
        this.nik = nik;
        this.no_kk = no_kk;
        this.tanggal = tanggal;
        this.bulan = bulan;
        this.tahun = tahun;
        this.email = email;
        this.password = password;
        
    }

    
    
    
    //SETTER
    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public void setNo_kk(String no_kk) {
        this.no_kk = no_kk;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    
    
    
    //GETTER
    public String getNama() {
        return nama;
    }

    public String getNik() {
        return nik;
    }

    public String getNo_kk() {
        return no_kk;
    }

    public String getTanggal() {
        return tanggal;
    }

    public String getBulan() {
        return bulan;
    }

    public String getTahun() {
        return tahun;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    
    
}
