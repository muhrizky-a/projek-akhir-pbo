package data;

abstract class DataAbstrak {
    abstract void tambahData();
    abstract void lihatData();
    abstract void updateData();
    abstract void hapusData();
    
}
